## qssi-user 11 RKQ1.200826.002 V13.0.3.0.RKXMIXM release-keys
- Manufacturer: xiaomi
- Platform: msmnile
- Codename: nabu
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V13.0.3.0.RKXMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/nabu_global/nabu:11/RKQ1.200826.002/V13.0.3.0.RKXMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V13.0.3.0.RKXMIXM-release-keys
- Repo: xiaomi_nabu_dump_24653


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
